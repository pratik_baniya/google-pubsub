package com.Pubsub;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.ListSubscriptionsResponse;
import com.google.api.services.pubsub.model.ListTopicsResponse;
import com.google.api.services.pubsub.model.Subscription;
import com.google.api.services.pubsub.model.Topic;
import com.google.api.services.pubsub.model.AcknowledgeRequest;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.api.services.pubsub.model.PullRequest;
import com.google.api.services.pubsub.model.PullResponse;
import com.google.api.services.pubsub.model.ReceivedMessage;
public class Sub {
	
	 /*
     * List topics in your project
     * */
    public static void listTopics( Pubsub pubsub) throws IOException{
    	
    	Pubsub.Projects.Topics.List listMethod =
    	        pubsub.projects().topics().list("projects/massive-seer-107519");
    	String nextPageToken = null;
    	ListTopicsResponse response;
    	do {
    	    if (nextPageToken != null) {
    	        listMethod.setPageToken(nextPageToken);
    	    }
    	    response = listMethod.execute();
    	    List<Topic> topics = response.getTopics();
    	    if (topics != null) {
    	        for (Topic topic : topics) {
    	            System.out.println("Found topic: " + topic.getName());
    	        }
    	    }
    	    nextPageToken = response.getNextPageToken();
    	} while (nextPageToken != null);
    }//listTopics
    
    /*
     * Subscribe to a Topic 
     * */
    public static void subscribeTopic( Pubsub pubsub) throws IOException{
    	 Subscription subscription = new Subscription()
         // The name of the topic from which this subscription
         // receives messages
         .setTopic("projects/massive-seer-107519/topics/newTopic")
         // Acknowledgement deadline in second
         .setAckDeadlineSeconds(30);
    	 
		 Subscription newSubscription = pubsub.projects().subscriptions().create(
		         "projects/massive-seer-107519/subscriptions/Test3", subscription)
		         .execute();
		 System.out.println("Created: " + newSubscription.getName());
    	
    }//subscribeTopic
    
    /*
     * Receiving Pull Messages
     * */
    
    /*
     * Receiving Pull Messages
     * *//*
    public static List<String> pullMessages(Pubsub pubsub)throws IOException{
  	
    	String subscriptionName =
    	        "projects/massive-seer-107519/subscriptions/testing";
    	// You can fetch multiple messages with a single API call.
    	int batchSize = 5;
    	PullRequest pullRequest = new PullRequest()
    	        // Setting ReturnImmediately to false instructs the API to
    	        // wait to collect the message up to the size of
    	        // MaxEvents, or until the timeout.
    	        .setReturnImmediately(false)
    	        .setMaxMessages(batchSize);
    	do {
    	    PullResponse pullResponse = pubsub.projects().subscriptions()
    	            .pull(subscriptionName, pullRequest).execute();
    	    List<String> ackIds = new ArrayList<>(batchSize);
    	    List<ReceivedMessage> receivedMessages =
    	            pullResponse.getReceivedMessages();
    	    List<String> message = new ArrayList<>();
    	    if (receivedMessages == null || receivedMessages.isEmpty()) {
    	        // The result was empty.
    	    	message = null;
    	        System.out.println("There were no messages.");
    	        continue;
    	    }
 
    	    System.out.println("Pull Message/ received Message size:" + receivedMessages.size());
    	    for (ReceivedMessage receivedMessage : receivedMessages) {
    	        PubsubMessage pubsubMessage = receivedMessage.getMessage();
    	        if (pubsubMessage != null) {
    	            System.out.print("Message: ");
    	            System.out.println(
    	                    new String(pubsubMessage.decodeData(), "UTF-8"));
    	            message.add(new String(pubsubMessage.decodeData(), "UTF-8"));
    	        }
    	        ackIds.add(receivedMessage.getAckId());	      
    	    }
    	    
    	    return  message;
    	} while (false);
    	return null;
    }//PullMessages*/
    
    public static void pullMessages(Pubsub pubsub)throws IOException{
    	String subscriptionName =
    	        "projects/massive-seer-107519/subscriptions/Test3";
    	// You can fetch multiple messages with a single API call.
    	int batchSize = 5;
    	PullRequest pullRequest = new PullRequest()
    	        // Setting ReturnImmediately to false instructs the API to
    	        // wait to collect the message up to the size of
    	        // MaxEvents, or until the timeout.
    	        .setReturnImmediately(false)
    	        .setMaxMessages(batchSize);
    	do {
    	    PullResponse pullResponse = pubsub.projects().subscriptions()
    	            .pull(subscriptionName, pullRequest).execute();
    	    List<String> ackIds = new ArrayList<>(batchSize);
    	    List<ReceivedMessage> receivedMessages =
    	            pullResponse.getReceivedMessages();
    	    
    	    List<String> message = new ArrayList<>();
    	    if (receivedMessages == null || receivedMessages.isEmpty()) {
    	        // The result was empty.
    	    	message = null;
    	        System.out.println("There were no messages.");
    	        continue;
    	    }
    	    for (int i = 0; i < receivedMessages.size(); i++)
    	    {
    	    	System.out.println( receivedMessages.get(i).getMessage());
    	    }
    	    
    	    System.out.println("Pull Message/ received Message size:" + receivedMessages.size());
    	    for (ReceivedMessage receivedMessage : receivedMessages) {
    	        PubsubMessage pubsubMessage = receivedMessage.getMessage();
    	        if (pubsubMessage != null) {
    	            System.out.print("Message: ");
    	            System.out.println(
    	                    new String(pubsubMessage.decodeData(), "UTF-8"));
    	          
    	            message.add(new String(pubsubMessage.decodeData(), "UTF-8"));
    	        }
    	        ackIds.add(receivedMessage.getAckId());	    
    	    
    	    }
    	    /*
    	    //message is added to Jedis
    	    if (message != null)
    	    	Jedis.addHash(pubsub, message);
    	   */
    	    // Ack can be done asynchronously if you care about throughput.
    	    AcknowledgeRequest ackRequest =
    	            new AcknowledgeRequest().setAckIds(ackIds);
    	    pubsub.projects().subscriptions()
    	            .acknowledge(subscriptionName, ackRequest).execute();
    	    // You can keep pulling messages by changing the condition below.
    	} while (false);
	
    }//PullMessages
  
    /*
     * Get the list of Subscription
     * */
    public static void SubscriptionList(Pubsub pubsub) throws IOException
    {
    	Pubsub.Projects.Subscriptions.List listMethod = pubsub.projects()
    	        .subscriptions().list("projects/massive-seer-107519");
    	String nextPageToken = null;
    	ListSubscriptionsResponse response;
    	do {
    	    if (nextPageToken != null) {
    	        listMethod.setPageToken(nextPageToken);
    	    }
    	    response = listMethod.execute();
    	    List<Subscription> subscriptions = response.getSubscriptions();
    	    if (subscriptions != null) {
    	        for (Subscription subscription : subscriptions) {
    	            System.out.println(
    	                    "Found subscription: " + subscription.getName());
    	        }
    	    }
    	    nextPageToken = response.getNextPageToken();
    	} while (nextPageToken != null);
    }//SubscriptionList
    	
    
}//Subscriber class

