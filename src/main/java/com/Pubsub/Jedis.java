package com.Pubsub;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisException;

import com.Pubsub.Sub;
import com.google.api.services.pubsub.Pubsub;

public class Jedis {
	
	private static final String host = "localhost";
	//Jedis pool connection
	private static JedisPool pool = null;
	
	public Jedis(){
		pool = new JedisPool(host);
	
	}//Jedis
	
	/*
	 * Receives the message from */
	public static void addHash(Pubsub pubsub, List<String> pullmessages)throws IOException, JedisException{
		//adding values in Redis Hash
		String key ="sensorID";
		Map<String, String> map = new HashMap<>();
		List<String> message =  pullmessages;
		System.out.println("Messages Stored in Redis for the key: " + key);
		for (int i = 0; i < message.size(); i++){
			map.put("r"+i, message.get(i));
			
		}
		redis.clients.jedis.Jedis jedis = pool.getResource();
		
		//save to redis
		jedis.hmset(key, map);
		
		//after saving the data, we are retreving the data 
		Map<String, String> retrieveMap = jedis.hgetAll(key);  
        for (String keyMap : retrieveMap.keySet()) {  
            System.out.println(keyMap + " " + retrieveMap.get(keyMap));  
        }  

	}
	
	public static void save(Pubsub pubsub) throws IOException{
		
		//List<String> mEssages = Sub.pullMessages(pubsub);	
		//addHash(pubsub, mEssages);
		
	}

}
