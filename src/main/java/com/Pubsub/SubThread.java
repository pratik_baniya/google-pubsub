package com.Pubsub;

import java.io.IOException;
import java.util.List;

import com.google.api.services.pubsub.Pubsub;

public class SubThread extends Thread {

	 private  boolean cancelled;
	 Pubsub pubsub;
	 private static List<String> mEssages;

	 
	public SubThread(Pubsub pbsub) {
	
		pubsub = pbsub;	
	}


	@Override
	public void run() {
		
		while (!cancelled){	
				try {
					sleep(10000);
					Sub.pullMessages( pubsub);
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
				
		}
		
	}
	 public static List<String> getMessages(){
		return mEssages;
	}
	   public void cancel()
	   {
	      cancelled = true;  
	   }

	   public boolean isCancelled() {
	      return cancelled;
	   }
}
