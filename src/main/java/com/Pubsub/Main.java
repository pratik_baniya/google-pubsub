package com.Pubsub;

import java.io.IOException;

//import redis.clients.jedis.Jedis;

import com.google.api.services.pubsub.Pubsub;
import com.Pubsub.Pub;
import com.Pubsub.Sub;

public class Main extends PortableConfiguration {

public static void main(String[] args) throws IOException {
		
	
	    //creating Pubsub Client
		Pubsub pubsub =   createPubsubClient();
		
		// setupTopic(pubsub);//Creates a new topic
	
		System.out.println("View ll the topics");
		Sub.listTopics(pubsub);
			
		//subscribeTopic(pubsub); // subscribes to a topic	
				
		//allows users to subscribe to a given topic 
		//Sub.subscribeTopic(pubsub);
			
		System.out.println("All the subscription");
		//displays all the subscription for a given topic
		Sub.SubscriptionList(pubsub);	
		
		//Connecting to Redis server on localhost
	    Jedis jedis = new Jedis();
	
		//Create/Publishes Message every 10 seconds to a topic 
		PubThread t1 = new PubThread(pubsub, "newTopic");
		t1.start();
		
		//Pull request where the message is received every 20 seconds
		SubThread t2 = new SubThread(pubsub);
		t2.start();
		
		//Save the it in the 
		//JedisThread t3 = new JedisThread(pubsub);
		//t3.start();
		
		
	 
		 //Pull request where the message is received saved to Jedis and then acknowledged
		 //Sub.pullMessages(pubsub);

	    }//end of main
    
}//main class
