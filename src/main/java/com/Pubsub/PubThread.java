package com.Pubsub;

import java.io.IOException;

import com.google.api.services.pubsub.Pubsub;

public class PubThread extends Thread{
	
	 private  boolean cancelled;
	 Pubsub pubsub;
	 String topic="";
	 
	public PubThread(Pubsub pbsub, String newtopic) {
		// TODO Auto-generated constructor stub
		pubsub = pbsub;
		topic = newtopic;	
	}

	
	@Override
	public void run() {
		
		while (!cancelled){
				
				try {
					
					Pub.MultipleMessageToTopic(pubsub, topic);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	}
	   public void cancel()
	   {
	      cancelled = true;  
	   }

	   public boolean isCancelled() {
	      return cancelled;
	   }
}
