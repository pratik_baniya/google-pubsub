package com.Pubsub;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.PublishRequest;
import com.google.api.services.pubsub.model.PublishResponse;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.api.services.pubsub.model.Topic;
import com.google.common.collect.ImmutableList;

public class Pub {
	   //Variables for random string
	   static int leftLimit = 97; // letter 'a'
	   static int rightLimit = 122; // letter 'z'
	   static  int targetStringLength = 8;
	   
	   static String  generatedString;

	 /*
	 * Create a new Topic
	 * */
	 private static void setupTopic (final Pubsub client) throws IOException {
	        
	    // Create the topic if it doesn't exist
	    Topic newTopic =   (client).projects().topics()
	                       .create("projects/massive-seer-107519/topics/mytopic4", new Topic())
	                       .execute();
	    System.out.println("Created: " + newTopic.getName());
	   
	 }//setupTopic
	 
	  /*
	  * Create a message to Topic
	  * */
	  public static void messagetoTopic(Pubsub pubsub, String topic, String temp) throws IOException{

	    String message = temp;
	    PubsubMessage pubsubMessage = new PubsubMessage();
	    // You need to base64-encode your message with
	    // PubsubMessage#encodeData() method.
	    pubsubMessage.encodeData(message.getBytes("UTF-8"));
	    List<PubsubMessage> messages = ImmutableList.of(pubsubMessage);
	    PublishRequest publishRequest =
	            new PublishRequest().setMessages(messages);
	    PublishResponse publishResponse = pubsub.projects().topics()
	            .publish("projects/massive-seer-107519/topics/"+ topic, publishRequest)
	            .execute();
	    
	    for (int i = 0; i < messages.size(); i++)
	    System.out.println(messages.get(i).getData());
	    
	    List<String> messageIds = publishResponse.getMessageIds();
	    if (messageIds != null) {
	        for (String messageId : messageIds) {
	            System.out.println("messageId: " + messageId);
	        }
	    }
	    }//messagetoTopic	
	  
	  /*
	   * Random String Generator
	   * */  
	  public static String randomString() {
		 StringBuilder buffer = new StringBuilder(targetStringLength);
		generatedString = "";  
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (new Random().nextFloat() * (rightLimit - leftLimit));
	        buffer.append((char) randomLimitedInt);
	    }
	    generatedString = buffer.toString();
	    return generatedString;
	   
	}
	  
	  public static void MultipleMessageToTopic(Pubsub pubsub, String topic) throws IOException{
		 String temp;
		 System.out.println("Messages Published");//Print
		 for (int i = 0; i < 5; i++)
		 {
			 temp = randomString();
			 
			 messagetoTopic( pubsub,  topic, temp);
		 }
		  
	  }
}//Publisher Class
