package com.Pubsub;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.util.Utils;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpStatusCodes;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.PubsubScopes;
import com.google.api.services.pubsub.model.Topic;

//import com.google.cloud.pubsub.client.demos.snippets.common.RetryHttpInitializerWrapper;
import com.Pubsub.RetryHttpInitializerWrapper;
import com.google.common.base.Preconditions;

import java.io.IOException;



import java.util.List;



//topic
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.Topic;
import com.google.api.services.pubsub.Pubsub;
//publish the message to topic
import com.google.api.services.pubsub.model.ListTopicsResponse;
import com.google.api.services.pubsub.model.PublishRequest;
import com.google.api.services.pubsub.model.PublishResponse;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.common.collect.ImmutableList;

//subscription

import com.google.api.services.pubsub.model.PushConfig;
import com.google.api.services.pubsub.model.Subscription;

//redis DB
import redis.clients.jedis.Jedis;
/**
 * Create a Pubsub client using portable credentials.
 */
public class PortableConfiguration {

    // Default factory method.
    public static Pubsub createPubsubClient() throws IOException {
        return createPubsubClient(Utils.getDefaultTransport(),
                Utils.getDefaultJsonFactory());
    }

    // A factory method that allows you to use your own HttpTransport
    // and JsonFactory.
    public static Pubsub createPubsubClient(HttpTransport httpTransport,
            JsonFactory jsonFactory) throws IOException {
        Preconditions.checkNotNull(httpTransport);
        Preconditions.checkNotNull(jsonFactory);
        GoogleCredential credential = GoogleCredential.getApplicationDefault(
                httpTransport, jsonFactory);
        // In some cases, you need to add the scope explicitly.
        if (credential.createScopedRequired()) {
            credential = credential.createScoped(PubsubScopes.all());
        }
        // Please use custom HttpRequestInitializer for automatic
        // retry upon failures.  We provide a simple reference
        // implementation in the "Retry Handling" section.
        HttpRequestInitializer initializer =
                new RetryHttpInitializerWrapper(credential);
        return new Pubsub.Builder(httpTransport, jsonFactory, initializer)
               .build();
    }
      
}//PortableConfiguration class